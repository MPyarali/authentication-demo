# Authentication Demo

## What this is for

This is a demonstration app on how to implement JWT Based Cookie authentication
in FastAPI.

## Dependencies

This uses bcrypt for password hashing and jose for JWT encoding and decoding

## Architecture

Uses fastAPI and Psycopg3 on the backend

Uses Vite + React + React Router on the frontend

## Running the project

Create a .env file based on the .env.sample file.

Set up the following environment variables in that file (the defaults should be
fine for testing purposes)

```shell
SIGNING_KEY
DATABASE_URL
POSTGRES_DB
POSTGRES_USER
POSTGRES_PASSWORD
```

Then just run:

`docker compose up -d`

## Notes on implementation

The frontend uses JSDoc to supply type annotations for the JavaScript and
includes the `// @ts-check` comment at the top which causes the TypeScript
Language Server in VSCode (and other editors) to attempt to do static type
checking of the JavaScript code.

The implementation has lots of comments to help you understand what it is doing.

## Files that could be reused in your project

### Backend

```text
api/utils/authentication.py - A library of helper functions for authentication
api/router/auth_router.py - A router for the authentication routes
api/models/users.py - Pydantic models for the user data
api/queries/user_queries.py - Database layer to talk to the users table
api/migrations/002_create_users.py - The migration which creates the users table
```

### Frontend

```text
ghi/src/components/AuthProvider.jsx - A React Context Provider to hold auth data
ghi/src/components/SignInForm.jsx - A signin form page
ghi/src/components/SignUpForm.jsx - A signup form page
ghi/src/hooks/useAuthService.js - A hook to get access to the logged in data
ghi/src/services/authService.js - A library containing code to call the backend auth API
```
